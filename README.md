# Oauth 1.0a Client Library

This crate implements the [OAuth Core v1.0a Specification](https://oauth.net/core/1.0a/)

## Features
- `rsa` - enables the `RSA-SHA1` signature method

## Status / Roadmap
> **Note:** I currently don't plan to develop this library much further as it fits my needs.
> Feel free to send MRs or file issues though, if there's demand I can put some more work into it :)

- [x] Basic Functionality - Encoding, Signatures (v0.1.0)
- [x] Handling Request Token Acquisition / Exchange (v0.1.0)
- [ ] Built-in optional integration with [`hyper`](https://crates.io/crates/hyper) - currently network layer left up to the user

## License
Licensed under the [EUPL-1.2](https://joinup.ec.europa.eu/collection/eupl/introduction-eupl-licence).

### Summary
> The licence is interoperable (no restrictions on linking in order to facilitate the integration of multiple components), reciprocal (third parties distributing improvements or derivatives must publish and provide back the modified source code) and compatible: no global relicensing permitted, but the source code could be reused in other projects under GPL/AGPL, EPL, LGPL, MPL, OSL, CeCILL, LiLiQ. EUPL covers SaaS / network distribution. EUPL covers "the Work" (software and ancillary data).
> [(Source)](https://joinup.ec.europa.eu/collection/eupl/solution/joinup-licensing-assistant/jla-find-and-compare-software-licenses)

## Contributing
Unless explicitly stated otherwise, you agree to release any contributions submitted for inclusion with this work under the `EUPL-1.2` license, without any additional terms or conditions.
