//! # Token Acquisition Flow Module
//! This module assists with acquisition of an [AccessToken] every time a user authorizes your app.
//!
//! You must already have [ConsumerCredentials], see your service provider's documentation for more
//! information.
//!
//! ## The Flow
//! The typical token acquisition flow consists of following steps:
//! 1. Acquiring a request token ([create_request_token_request]).
//!    If the server uses `application/x-www-form-urlencoded` content type, you can use the
//!    [get_request_token_from_response] helper function.
//! 2. Redirecting the user to the service provider's authentication page ([create_request_uri]).
//!    You will receive the `oauth_verifier`, either via the callback or a code displayed to the user.
//! 3. Exchanging the [RequestToken] and `oauth_verifier` for an [AccessToken]
//!    ([create_token_exchange_request]).
//!    If the server uses `application/x-www-form-urlencoded` content type, you can use the
//!    [get_access_token_from_response] helper function.

use crate::*;
use std::collections::HashMap;

/// Internally the same as AccessToken, but the distinction is made for easier understanding
pub type RequestToken = AccessToken;

/// Creates an [OAuthRequest] to acquire a request token
///
/// If callback is none, sets `oauth_callback` to `oob`
pub fn create_request_token_request(
    uri: Uri,
    callback: Option<Uri>,
    consumer: ConsumerCredentials,
    extra_params: Box<[Parameter]>,
) -> Result<OAuthRequest, Box<dyn std::error::Error>> {
    OAuthRequest::builder(
        uri,
        AuthenticationLevel::Consumer(consumer),
        Box::new(signature::HmacSha1Signature),
    )
    .add_auth_parameters(&[Parameter::new(
        "oauth_callback",
        callback
            .as_ref()
            .map(Uri::to_string)
            .unwrap_or("oob".to_string()),
    )])
    .add_auth_parameters(&extra_params)
    .build()
}

/// Returns the [RequestToken] and any additional parameters
pub fn get_request_token_from_response(
    response: &str,
) -> Result<(RequestToken, HashMap<String, String>), Box<dyn std::error::Error>> {
    let mut parameters = HashMap::new();

    for parameter in response.split('&') {
        let (key, value) = parameter
            .split_once('=')
            .ok_or(OAuthError::InvalidResponse)?;
        let key = percent_encoding::decode_string(key)?;
        let value = percent_encoding::decode_string(value)?;

        parameters.insert(key, value);
    }

    match parameters.remove("oauth_callback_confirmed") {
        Some(c) if c == "true" => Ok(()),
        None | Some(_) => Err(OAuthError::CallbackNotConfirmed),
    }?;
    let token = parameters
        .remove("oauth_token")
        .ok_or(OAuthError::InvalidResponse)?;
    let secret = parameters
        .remove("oauth_token_secret")
        .ok_or(OAuthError::InvalidResponse)?;

    Ok((RequestToken::new(&token, &secret), parameters))
}

/// Creates the request [Uri] you should present to your user
pub fn create_request_uri(
    uri: Uri,
    token: RequestToken,
    extra_params: Vec<Parameter>,
) -> Result<Uri, Box<dyn std::error::Error>> {
    // TODO: half of this is literally copied from a the request builder, that's like, bad
    let mut params = extra_params;

    params.push(Parameter::new("oauth_token", token.token));

    params.sort();

    let mut uri = uri.into_parts();

    let params = params
        .iter()
        .map(|p| p.encoded())
        .intersperse(String::from('&'))
        .collect::<String>();

    match uri.path_and_query {
        None => uri.path_and_query = Some(PathAndQuery::from_str(&(String::from("?") + &params))?),
        Some(pq) => match pq.query() {
            None => {
                uri.path_and_query = Some(PathAndQuery::from_str(&format!(
                    "{}?{}",
                    pq.path(),
                    params
                ))?)
            }
            Some(q) => {
                uri.path_and_query = Some(PathAndQuery::from_str(&format!(
                    "{}?{}&{}",
                    pq.path(),
                    q,
                    params
                ))?)
            }
        },
    };

    Ok(Uri::from_parts(uri)?)
}

/// Creates the token exchange request to exchange the [RequestToken] and verifier string for an [AccessToken]
pub fn create_token_exchange_request(
    uri: Uri,
    verifier: String,
    request_token: RequestToken,
    consumer: ConsumerCredentials,
    extra_params: Box<[Parameter]>,
) -> Result<OAuthRequest, Box<dyn std::error::Error>> {
    OAuthRequest::builder(
        uri,
        AuthenticationLevel::Token(consumer, request_token),
        Box::new(HmacSha1Signature),
    )
    .add_auth_parameters(&[Parameter::new("oauth_verifier", verifier)])
    .add_auth_parameters(&extra_params)
    .build()
}

/// Returns the [AccessToken] and any additional parameters
pub fn get_access_token_from_response(
    response: &str,
) -> Result<(AccessToken, HashMap<String, String>), Box<dyn std::error::Error>> {
    let mut parameters = HashMap::new();

    for parameter in response.split('&') {
        let (key, value) = parameter
            .split_once('=')
            .ok_or(OAuthError::InvalidResponse)?;
        let key = percent_encoding::decode_string(key)?;
        let value = percent_encoding::decode_string(value)?;

        parameters.insert(key, value);
    }

    let token = parameters
        .remove("oauth_token")
        .ok_or(OAuthError::InvalidResponse)?;
    let secret = parameters
        .remove("oauth_token_secret")
        .ok_or(OAuthError::InvalidResponse)?;

    Ok((AccessToken::new(&token, &secret), parameters))
}
