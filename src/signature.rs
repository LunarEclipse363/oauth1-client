//! This module contains all the [SignatureMethod]s specified by the standard.

use crate::parameter::Parameter;
use crate::percent_encoding::encode_string;
use crate::AuthenticationLevel;

use std::error::Error;
use std::fmt::Debug;

use base64::engine::{general_purpose as base64_engine, Engine as _};
use dyn_clonable::clonable;
use hmac::{Hmac, Mac};
use sha1::Sha1;

#[cfg(feature = "rsa")]
pub use mod_rsa::*;

/// The trait for signature methods
#[clonable]
pub trait SignatureMethod: Debug + Clone {
    /// Returns the name of the method
    ///
    /// For example `HMAC-SHA1`
    fn name(&self) -> &'static str;

    /// Returns the `oauth_signature_method` parameter
    fn as_parameter(&self) -> Parameter {
        Parameter {
            name: String::from("oauth_signature_method"),
            value: String::from(self.name()),
        }
    }

    /// Attempts to sign the provided base string, and returns the `oauth_signature` parameter.
    fn sign(
        &self,
        base_string: &str,
        auth: &AuthenticationLevel,
    ) -> Result<Parameter, Box<dyn Error>>;
}

/// `PLAINTEXT` signature method.
#[derive(Clone, Copy, Debug)]
pub struct PlaintextSignature;
impl SignatureMethod for PlaintextSignature {
    fn name(&self) -> &'static str {
        "PLAINTEXT"
    }

    fn sign(
        &self,
        _base_string: &str,
        auth: &AuthenticationLevel,
    ) -> Result<Parameter, Box<dyn Error>> {
        let consumer_secret = &match auth {
            AuthenticationLevel::Consumer(c) => c.secret.clone(),
            AuthenticationLevel::Token(c, _t) => c.secret.clone(),
        };
        let token_secret = if let AuthenticationLevel::Token(_, t) = auth {
            &t.secret
        } else {
            ""
        };

        Ok(Parameter {
            name: String::from("oauth_signature"),
            value: format!(
                "{}&{}",
                encode_string(consumer_secret),
                encode_string(token_secret)
            ),
        })
    }
}

/// `HMAC-SHA1` signature method.
#[derive(Clone, Copy, Debug)]
pub struct HmacSha1Signature;
impl SignatureMethod for HmacSha1Signature {
    fn name(&self) -> &'static str {
        "HMAC-SHA1"
    }

    fn sign(
        &self,
        base_string: &str,
        auth: &AuthenticationLevel,
    ) -> Result<Parameter, Box<dyn Error>> {
        let consumer_secret = &match auth {
            AuthenticationLevel::Consumer(c) => c.secret.clone(),
            AuthenticationLevel::Token(c, _t) => c.secret.clone(),
        };
        let token_secret = if let AuthenticationLevel::Token(_, t) = auth {
            &t.secret
        } else {
            ""
        };

        Ok(Parameter {
            name: String::from("oauth_signature"),
            value: {
                let key = format!("{}&{}", consumer_secret, token_secret);
                let mut mac: Hmac<Sha1> = Hmac::new_from_slice(key.as_bytes())?;
                mac.update(base_string.as_bytes());
                let signature = mac.finalize().into_bytes();

                base64_engine::STANDARD.encode(signature)
            },
        })
    }
}

#[cfg(feature = "rsa")]
mod mod_rsa {
    use super::*;
    use rand::thread_rng;
    use rsa::{pkcs1v15::SigningKey, signature::RandomizedSigner, RsaPrivateKey};

    /// `RSA-SHA1` signature method.
    #[derive(Clone, Debug)]
    pub struct RsaSha1Signature(RsaPrivateKey);
    impl SignatureMethod for RsaSha1Signature {
        fn name(&self) -> &'static str {
            "RSA-SHA1"
        }

        fn sign(
            &self,
            base_string: &str,
            _auth: &AuthenticationLevel,
        ) -> Result<Parameter, Box<dyn Error>> {
            Ok(Parameter {
                name: String::from("oauth_signature"),
                value: {
                    let signing_key: SigningKey<Sha1> = SigningKey::new_with_prefix(self.0.clone());
                    let signature =
                        signing_key.try_sign_with_rng(&mut thread_rng(), base_string.as_bytes())?;

                    base64_engine::STANDARD.encode(signature)
                },
            })
        }
    }
}
