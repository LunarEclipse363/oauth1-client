//! Module with methods for handling RFC3986 percent encoding
//! with an OAuth1.0a specific reserved characters set

use rfc3986::{percent_decode_str, utf8_percent_encode, AsciiSet, NON_ALPHANUMERIC};

/// Reserved characters set for the RFC3986 percent encoding
pub const RESERVED_CHARACTERS: &AsciiSet = &NON_ALPHANUMERIC
    .remove(b'-')
    .remove(b'.')
    .remove(b'_')
    .remove(b'~');

/// Encode string with the RFC3986 percent encoding with the reserved characters set specified in OAuth1.0a
pub fn encode_string(input: &str) -> String {
    utf8_percent_encode(input, RESERVED_CHARACTERS).to_string()
}

/// Decode string with the RFC3986 percent encoding with the reserved characters set specified in OAuth1.0a
pub fn decode_string(input: &str) -> Result<String, Box<dyn std::error::Error>> {
    Ok(percent_decode_str(input)?.decode_utf8()?.to_string())
}
