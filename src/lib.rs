//! # OAuth1.0a Client Library
//!
//! ## Overview
//! This library handles OAuth1.0a in a network-backend agnostic way.  
//! The current intended usage is that you implement your own code to send an [OAuthRequest]
//! with whichever networking library you chose.
//!
//! It handles all the difficult parts of the process, letting you focus on just sending your data.
//!
//! ```
//! use http::Uri;
//! use oauth1_client::*;
//!
//! let consumer = ConsumerCredentials::new(
//!     "example_consumer_key",
//!     "example_consumer_secret"
//! );
//!
//! let token = AccessToken::new(
//!     "example_token",
//!     "example_token_secret"
//! );
//!
//! let request = OAuthRequest::builder(
//!     Uri::from_static("https://example.com/api"),
//!     AuthenticationLevel::Token(consumer, token),
//!     Box::new(HmacSha1Signature)
//! )
//! .scheme(AuthorizationScheme::Header)
//! .add_auth_parameters(&[Parameter::new("realm", "http://sp.example.com/")])
//! .add_parameters(&[
//!     Parameter::new("message", "Hello World!"),
//!     Parameter::new("extra", "meow"),
//! ])
//! .build()
//! .unwrap();
//!
//! // Now you can send the request
//! ```
//!
//! ## Basic Abstractions
//!
//! ### Authentication Levels
//! There are two [AuthenticationLevel]s a request can have:
//! - [AuthenticationLevel::Consumer] - contains only [ConsumerCredentials] - you're identifying as a specific application, but acting on
//! your own behalf
//! - [AuthenticationLevel::Token] - contains [ConsumerCredentials] and an [AccessToken] - you're identifying as a specific application and specific
//! user, and acting on behalf of that user
//!
//! ### Signature Methods
//! - [signature::HmacSha1Signature] - A basic signature created using the consumer secret and
//! token secret.
//! - [signature::PlaintextSignature] - **Insecure.** Instead of a signature,
//! sends the consumer and token secrets in plaintext.
//! - [signature::RsaSha1Signature] - Signs the request with an RSA private key. Requires the `rsa`
//! crate feature to be enabled.
//!
//! ### Authorization Schemes
//! The [AuthorizationScheme] decides how to structure the request.
//! The default is [AuthorizationScheme::Header] as it is recommended by the standard.
//! See its documentation page for more details.
//!
//! ## Authentication Flow
//! See the module-level documentation for [flow].

#![allow(unstable_name_collisions)]

pub mod flow;
pub mod percent_encoding;
pub mod signature;

pub use error::OAuthError;
pub use parameter::Parameter;
pub use signature::*;

mod error;
mod parameter;

use parameter::has_unique_parameters;
use percent_encoding::*;

use std::str::FromStr;
use std::time::SystemTime;

use http::{
    method::Method as HttpMethod,
    uri::{Authority, PathAndQuery, Scheme},
    HeaderMap, HeaderValue, Uri,
};
use itertools::Itertools;
use uuid::Uuid;

/// A ready-to-send request
#[derive(Debug, Clone, PartialEq, Eq)]
pub struct OAuthRequest {
    /// The request Uri
    pub uri: Uri,
    /// The request Headers
    pub headers: HeaderMap,
    /// The HTTP method to be used, either `GET` or `POST`
    pub method: HttpMethod,
    /// The body of the request when applicable
    pub body: Option<String>,
}

/// Consumer key and secret, required for all OAuth requests
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct ConsumerCredentials {
    pub key: String,
    pub secret: String,
}

impl ConsumerCredentials {
    pub fn new(key: &str, secret: &str) -> Self {
        Self {
            key: key.into(),
            secret: secret.into(),
        }
    }
}

/// A token and token secret, required for authorized requests
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub struct AccessToken {
    pub token: String,
    pub secret: String,
}

impl AccessToken {
    pub fn new(token: &str, secret: &str) -> Self {
        Self {
            token: token.into(),
            secret: secret.into(),
        }
    }
}

/// Whether this request is authorized or not
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum AuthenticationLevel {
    /// An unauthorized request, using just consumer credentials
    Consumer(ConsumerCredentials),
    /// An authorized request, using consumer credentials and an access token
    Token(ConsumerCredentials, AccessToken),
}

/// How exactly should the request be made
#[derive(Debug, Clone, PartialEq, Eq, Hash)]
pub enum AuthorizationScheme {
    /// Use the Authorization header for OAuth, use the request body for everything else. Uses a
    /// POST request.
    Header,
    /// Put everything in the body of a POST request.
    Body,
    /// Put everything in the Uri parameters of a GET request.
    Uri,
}

impl Default for AuthorizationScheme {
    fn default() -> Self {
        Self::Header
    }
}

impl OAuthRequest {
    /// Returns an [OAuthRequestBuilder].
    pub fn builder(
        request_uri: Uri,
        auth_level: AuthenticationLevel,
        signature_method: Box<dyn SignatureMethod>,
    ) -> OAuthRequestBuilder {
        OAuthRequestBuilder::new(request_uri, auth_level, signature_method)
    }
}

/// A builder for [OAuthRequest]
#[derive(Debug, Clone)]
pub struct OAuthRequestBuilder {
    request_uri: Uri,
    auth_level: AuthenticationLevel,
    signature_method: Box<dyn SignatureMethod>,
    scheme: AuthorizationScheme,
    parameters: Vec<Parameter>,
    extra_auth: Vec<Parameter>,
}

impl OAuthRequestBuilder {
    /// Returns a new [OAuthRequestBuilder]
    ///
    /// The default settings are [AuthorizationScheme::Header] and no extra parameters.
    pub fn new(
        request_uri: Uri,
        auth_level: AuthenticationLevel,
        signature_method: Box<dyn SignatureMethod>,
    ) -> Self {
        OAuthRequestBuilder {
            request_uri,
            auth_level,
            signature_method,
            scheme: AuthorizationScheme::default(),
            parameters: vec![],
            extra_auth: vec![],
        }
    }

    /// The parameters added here will be added as body or uri parameters, depending on the scheme
    pub fn add_parameters(mut self, parameters: &[Parameter]) -> Self {
        self.parameters.extend_from_slice(parameters);
        self
    }

    /// The parameters added with this will be added as headers if [AuthorizationScheme::Header]
    ///
    /// Useful for adding the `realm` parameter if your use case requires it
    pub fn add_auth_parameters(mut self, parameters: &[Parameter]) -> Self {
        self.extra_auth.extend_from_slice(parameters);
        self
    }

    /// Sets the [AuthorizationScheme] to use
    pub fn scheme(mut self, scheme: AuthorizationScheme) -> Self {
        self.scheme = scheme;
        self
    }

    /// Attempts to create a request with the provided settings.
    ///
    /// Notably, returns an error if there are any duplicated parameters.
    /// Can also return other errors, if for example signing the request fails.
    pub fn build(self) -> Result<OAuthRequest, Box<dyn std::error::Error>> {
        let mut auth_params = self.extra_auth;
        let mut user_params = self.parameters;

        // Remove realm from auth_params since it's skipped in the base string generation
        let mut realm = None;
        auth_params = auth_params
            .into_iter()
            .filter_map(|p| {
                if p.name == "realm" {
                    realm = Some(p);
                    None
                } else {
                    Some(p)
                }
            })
            .collect();

        auth_params.push(Parameter {
            name: String::from("oauth_version"),
            value: String::from("1.0"),
        });
        auth_params.push(Parameter {
            name: String::from("oauth_timestamp"),
            value: format!(
                "{}",
                SystemTime::now()
                    .duration_since(SystemTime::UNIX_EPOCH)
                    .unwrap()
                    .as_secs()
            ),
        });
        auth_params.push(Parameter {
            name: String::from("oauth_nonce"),
            value: Uuid::new_v4().hyphenated().to_string(),
        });
        auth_params.push(self.signature_method.as_parameter());
        {
            let (AuthenticationLevel::Consumer(c) | AuthenticationLevel::Token(c, _)) =
                &self.auth_level;
            auth_params.push(Parameter {
                name: String::from("oauth_consumer_key"),
                value: c.key.clone(),
            });
        }
        if let AuthenticationLevel::Token(_, t) = &self.auth_level {
            auth_params.push(Parameter {
                name: String::from("oauth_token"),
                value: t.token.clone(),
            });
        }

        let mut all_params = auth_params.clone();
        all_params.extend_from_slice(&user_params);
        if !has_unique_parameters(all_params.iter()) {
            return Err(Box::new(OAuthError::DuplicateParameters));
        };
        all_params.sort();

        let base_method = match &self.scheme {
            AuthorizationScheme::Header | AuthorizationScheme::Body => "POST",
            AuthorizationScheme::Uri => "GET",
        };

        let base_uri = normalize_uri(self.request_uri.clone());

        let base_params = all_params
            .iter()
            .map(|p| p.encoded())
            .intersperse(String::from('&'))
            .collect::<String>();

        let base_string = format!(
            "{}&{}&{}",
            base_method,
            encode_string(&base_uri.to_string()),
            encode_string(&base_params)
        );

        let signature = self.signature_method.sign(&base_string, &self.auth_level)?;
        all_params.push(signature.clone());
        auth_params.push(signature);

        if let Some(r) = realm {
            all_params.push(r.clone());
            auth_params.push(r);
        }

        let mut headers = HeaderMap::new();

        match &self.scheme {
            AuthorizationScheme::Header => {
                auth_params.sort();
                user_params.sort();

                headers.insert(
                    "Authorization",
                    HeaderValue::from_str(&format!(
                        "OAuth {}",
                        &auth_params
                            .iter()
                            .map(|p| p.encoded())
                            .intersperse(String::from(','))
                            .collect::<String>(),
                    ))?,
                );

                headers.insert(
                    "Content-Type",
                    HeaderValue::from_static("application/x-www-form-urlencoded"),
                );

                let body = user_params
                    .iter()
                    .map(|p| p.encoded())
                    .intersperse(String::from('&'))
                    .collect::<String>();

                Ok(OAuthRequest {
                    uri: self.request_uri,
                    headers,
                    method: HttpMethod::POST,
                    body: Some(body),
                })
            }
            AuthorizationScheme::Body => {
                auth_params.sort();
                user_params.sort();

                headers.insert(
                    "Content-Type",
                    HeaderValue::from_static("application/x-www-form-urlencoded"),
                );

                let body = all_params
                    .iter()
                    .map(|p| p.encoded())
                    .intersperse(String::from('&'))
                    .collect::<String>();

                Ok(OAuthRequest {
                    uri: self.request_uri,
                    headers,
                    method: HttpMethod::POST,
                    body: Some(body),
                })
            }
            AuthorizationScheme::Uri => {
                all_params.sort();

                let mut uri = self.request_uri.into_parts();

                let params = all_params
                    .iter()
                    .map(|p| p.encoded())
                    .intersperse(String::from('&'))
                    .collect::<String>();

                match uri.path_and_query {
                    None => {
                        uri.path_and_query =
                            Some(PathAndQuery::from_str(&(String::from("?") + &params))?)
                    }
                    Some(pq) => match pq.query() {
                        None => {
                            uri.path_and_query = Some(PathAndQuery::from_str(&format!(
                                "{}?{}",
                                pq.path(),
                                params
                            ))?)
                        }
                        Some(q) => {
                            uri.path_and_query = Some(PathAndQuery::from_str(&format!(
                                "{}?{}&{}",
                                pq.path(),
                                q,
                                params
                            ))?)
                        }
                    },
                };

                Ok(OAuthRequest {
                    uri: Uri::from_parts(uri)?,
                    headers,
                    method: HttpMethod::GET,
                    body: None,
                })
            }
        }
    }
}

fn normalize_uri(original: Uri) -> Uri {
    let port = original.port_u16();
    let mut normalized_uri = original.into_parts();

    // Unwrap is safe because we're just converting a valid value to lowercase
    // FIXME: scheme seems to be normalized by the http libary already
    normalized_uri.scheme = normalized_uri
        .scheme
        .map(|s| Scheme::from_str(&s.as_str().to_lowercase()).unwrap());
    normalized_uri.authority = normalized_uri
        .authority
        .map(|a| Authority::from_str(&a.as_str().to_lowercase()).unwrap());

    // Unwrap is safe here because we're just deleting the port
    match &normalized_uri.scheme {
        Some(s)
            if s.as_str() == "http" && port == Some(80)
                || s.as_str() == "https" && port == Some(443) =>
        {
            normalized_uri.authority = normalized_uri
                .authority
                .map(|a| Authority::from_str(a.host()).unwrap());
        }
        _ => {}
    };

    normalized_uri.path_and_query = normalized_uri
        .path_and_query
        .map(|pq| pq.path().parse().unwrap());

    Uri::from_parts(normalized_uri).unwrap()
}
