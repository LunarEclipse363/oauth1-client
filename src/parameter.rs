use crate::percent_encoding::encode_string;

use std::collections::HashSet;
use std::fmt::Display;

/// OAuth protocol parameter - case-sensitive name + value
#[derive(Debug, Clone, PartialEq, PartialOrd, Eq, Ord)]
pub struct Parameter {
    pub name: String,
    pub value: String,
}

impl Parameter {
    /// Returns an RFC3986 percent-encoded representation of the parameter
    pub fn encoded(&self) -> String {
        format!(
            "{}={}",
            encode_string(&self.name),
            encode_string(&self.value)
        )
    }

    /// Creates a new [Parameter] with the given name and value
    pub fn new(name: impl Into<String>, value: impl Into<String>) -> Self {
        Self {
            name: name.into(),
            value: value.into(),
        }
    }
}

impl Display for Parameter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}={}", self.name, self.value)
    }
}

/// Checks if all the Parameters are unique
// https://stackoverflow.com/questions/46766560/how-to-check-if-there-are-duplicates-in-a-slice
pub(crate) fn has_unique_parameters<'a, T: IntoIterator<Item = &'a Parameter>>(iter: T) -> bool {
    let mut uniq = HashSet::new();
    iter.into_iter().all(|x| uniq.insert(&x.name))
}
