use thiserror::Error;

/// Error types specific to this crate
#[derive(Error, Debug)]
pub enum OAuthError {
    /// Some provided parameters were duplicated or duplicated internal ones
    #[error("some provided parameters were duplicated or duplicated internal ones")]
    DuplicateParameters,
    /// The response passed to a function in the flow module was invalid in some way
    #[error("the response passed to a function in the flow module was invalid in some way")]
    InvalidResponse,
    /// The response didn't contain `oauth_callback_confirmed` or it was set to false
    #[error("the response didn't contain `oauth_callback_confirmed` or it was set to false")]
    CallbackNotConfirmed,
}
